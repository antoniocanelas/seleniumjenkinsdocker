package seleniumexamples;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.testng.AssertJUnit.assertEquals;

public class GRID_CompraWortenTestNG {
    static String productDescription;
    static String productPriceCurrent;
    public WebDriver driver;
    public String URL, Node;
    protected ThreadLocal<RemoteWebDriver> threadDriver = null;
    JavascriptExecutor jse;

    @Parameters("browser")
    @BeforeTest
    public void launchapp(String browser) throws MalformedURLException {
        String URL = "http://www.worten.pt";

//	      System.setProperty("webdriver.chrome.driver",
//					"C:\\Users\\sergio.valente\\eclipse-workspace\\SeleniumExample\\drivers\\chromedriver.exe");
//
//	      System.setProperty("webdriver.firefox.driver",
//					"C:\\Users\\sergio.valente\\eclipse-workspace\\SeleniumExample\\drivers\\geckodriver.exe");

        if (browser.equalsIgnoreCase("firefox")) {
            System.out.println(" Executing on FireFox");
            String Node = "http://localhost:4444/wd/hub";
            FirefoxOptions cap = new FirefoxOptions();
            cap.setCapability("browserName", "firefox");

            driver = new RemoteWebDriver(new URL(Node), cap);
            // Puts an Implicit wait, Will wait for 10 seconds before throwing exception
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().window().setSize(new Dimension(1920, 1080));

            // Launch website
            driver.navigate().to(URL);
            driver.manage().window().maximize();
        } else if (browser.equalsIgnoreCase("chrome")) {
            System.out.println(" Executing on CHROME");
            FirefoxOptions cap = new FirefoxOptions();
            System.out.println("At� 57");
            cap.setCapability("browserName", "chrome");
            cap.setCapability("version", "68");
            System.out.println("At� 59");
            String Node = "http://localhost:4444/wd/hub";
            System.out.println("At� 61");
            driver = new RemoteWebDriver(new URL(Node), cap);
            System.out.println("At� 63");
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            System.out.println("At� 65");
            driver.manage().window().setSize(new Dimension(1920, 1080));

            // Launch website
            driver.navigate().to(URL);
            driver.manage().window().maximize();
        } else if (browser.equalsIgnoreCase("ie")) {
            System.out.println(" Executing on IE");
            DesiredCapabilities cap = DesiredCapabilities.chrome();
            cap.setBrowserName("ie");
            String Node = "http://10.112.66.52:5558/wd/hub";
            driver = new RemoteWebDriver(new URL(Node), cap);
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            // Launch website
            driver.navigate().to(URL);
            driver.manage().window().maximize();
        } else {
            throw new IllegalArgumentException("The Browser Type is Undefined");
        }
    }

    @Test
    public void stage01_testLogin() throws Exception {

        WebDriverWait wait = new WebDriverWait(driver, 20);

        System.out.println("Test1");
        assertEquals("Worten Online | Tudo o que precisa em Worten.pt - 24H", driver.getTitle());

        wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("INICIAR SESS�O"))).click();

        driver.findElement(By.cssSelector("input#email")).sendKeys("sergio134@sapo.pt");
        driver.findElement(By.cssSelector("input#pass")).sendKeys("wortenNotFree");
        driver.findElement(By.xpath("//*[@id=\"accountLogin\"]/div/div/div/div[1]/form/div[2]/div/button")).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button.dropbtn_login_menu")));
        driver.navigate().back();
        Thread.sleep(1000);

        assertEquals("https://www.worten.pt/cliente/conta#/myDashboard", driver.getCurrentUrl());
        assertEquals("�rea de Cliente | In�cio de Sess�o como Cliente | Worten.pt", driver.getTitle());
    }

    private void scrollDown() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000)");
    }

    @AfterTest
    public void closeBrowser() {
        driver.quit();
    }
}
